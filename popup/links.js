let site = 'https://links.karelcom.net';
// let site = 'http://links.karjala.local';

// find URL
let url;
browser.tabs.query({currentWindow: true, active: true})
    .then(tabs => {
        url = tabs[0].url;
    });

document.getElementById("save-to-links")
    .addEventListener("click", function(event) {
        const width = 600, height = 450;
        const windowFeatures = "menubar=no,toolbar=no,location=yes,resizable=yes,scrollbars=yes,status=no," +
            `left=${(screen.width - width) / 2},top=${(screen.height - height) / 2},width=${width},height=${height}`;
        window.open(
            `${site}/browser_plugin/save_link?url=${encodeURIComponent(url)}`,
            '_blank',
            windowFeatures,
        );
        window.close();
    });

document.getElementById("save-private-quickly")
    .addEventListener("click", function(event) {
        const width = 600, height = 350;
        const windowFeatures = "menubar=no,toolbar=no,location=yes,resizable=yes,scrollbars=yes,status=no," +
            `left=${(screen.width - width) / 2},top=${(screen.height - height) / 2},width=${width},height=${height}`;
        window.open(
            `${site}/api/browser_plugin/quick_add?url=${encodeURIComponent(url)}`,
            '_blank',
            windowFeatures,
        );
        window.close();
    });

document.getElementById("my-own-links")
    .addEventListener("click", function(event) {
        window.open(`${site}/own_links`);
        window.close();
    });
